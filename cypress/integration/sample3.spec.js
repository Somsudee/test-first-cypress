/// <reference types="Cypress"/>
describe('Fill the form test', () => {
    Cypress.on('uncaught:exception', (err, runnable) => {
        // returning false here prevents Cypress from
        //failing the test
        return false
    })

    it('Currency test input integer', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
        cy.get('#selectSource')
            .click()
        cy.get('#sourceAmount')
            .type('10')
        cy.get('#selectTarget')
            .click()
        cy.get('#targetUSD')
            .click()
        cy.get('#calBtn')
            .click()
        cy.get('#result').should('contain.text', '0.3333 USD')
    })

    //case2
    it('Currency test input zero', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
        cy.get('#selectSource')
            .click()
        cy.get('#sourceAmount')
            .type('0')
        cy.get('#selectTarget')
            .click()
        cy.get('#targetUSD')
            .click()
        cy.get('#calBtn')
            .click()
        cy.get('#result').should('contain.text', '0 USD')
    })

    //case3
    it('Currency test input string', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
        cy.get('#selectSource')
            .click()
        cy.get('#sourceAmount')
            .type('a')
        cy.get('#selectTarget')
            .click()
        cy.get('#targetUSD')
            .click()
        cy.get('#calBtn')
            .click()
        cy.get('#result').should('contain.text', ' USD')
    })

    //case4
    it('Currency test input space ', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
        cy.get('#selectSource')
            .click()
        cy.get('#sourceAmount')
            .type(' ')
        cy.get('#selectTarget')
            .click()
        cy.get('#targetUSD')
            .click()
        cy.get('#calBtn')
            .click()
        cy.get('#result').should('contain.text', '0.00 USD')
    })

    //case5
    it('Currency test input negative', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
        cy.get('#selectSource')
            .click()
        cy.get('#sourceAmount')
            .type('-10')
        cy.get('#selectTarget')
            .click()
        cy.get('#targetUSD')
            .click()
        cy.get('#calBtn')
            .click()
        cy.get('#result').should('contain.text', '-0.3333 USD')
    })

    //case6
    it('Currency test input negative', () => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
        cy.get('#selectSource')
            .click()
        cy.get('#sourceAmount')
            .type('*-+')
        cy.get('#selectTarget')
            .click()
        cy.get('#targetUSD')
            .click()
        cy.get('#calBtn')
            .click()
        cy.get('.label').should('be.visible')
        cy.get('#result').should('contain.text', ' USD')
    })

})