/// <reference types="Cypress"/>
describe('Fill the form test',() => {
    Cypress.on('uncaught:exception', (err, runnable) => {
        // returning false here prevents Cypress from
        //failing the test
        return false
    })

    it('Open itsc.cmu',() => {
        cy.visit('https://itsc.cmu.ac.th/')
        cy.get('.show > .modal-dialog > .modal-content > .modal-header > .close > span')
            .click()
        cy.get('#wucNavBar_lblServiceTitle')
            .click()
        cy.get('#wucNavBar_wucNavbarOnlineStoreage_rptNav_hlLink_1')
            .click()
    })
})