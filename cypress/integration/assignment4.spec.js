/// <reference types="Cypress"/>
describe('Fill the form test',() => {

    //case1 add task
    it('Add a new task',() => {
        cy.visit('http://localhost:8081/')
        //add first
        cy.get('[href="/task/add"]')
            .click()
        cy.get('#summary')
            .type('asdf')
        cy.get('#detail')
            .type('asdf')
        cy.get('#priority')
            .select('Normal')
        cy.get('#duedate')
            .type('2020-12-06')
        cy.get('.btn')
            .click()

        //add second
        cy.get('[href="/task/add"]')
            .click()
        cy.get('#summary')
            .type('Marathon')
        cy.get('#detail')
            .type('Wing lai lung')
        cy.get('#priority')
            .select('High')
        cy.get('#duedate')
            .type('2020-10-13')
        cy.get('.btn')
            .click()
        
        cy.get('.card-title').should('contain.text','asdf')
        cy.get(':nth-child(4) > .card-body > .card-text').should('contain.text','asdf')
        cy.get(':nth-child(4) > .card-header > .row > :nth-child(2) > .badge').should('contain.text','Normal')
        cy.get('.card-header > .row > :nth-child(1)').should('contain.text','2020-12-06')
        
        cy.get(':nth-child(2) > .card-footer > .row > :nth-child(2) > .nav > [cl=""] > .nav-link > .fas')
            .click()
        cy.get('.btn-danger').click()
        cy.get(':nth-child(2) > .card-footer > .row > :nth-child(2) > .nav > [cl=""] > .nav-link > .fas')
            .click()
        cy.get('.btn-danger').click()
        
    })

    //case2 delete task
    it('Delete a task',() => {
        cy.visit('http://localhost:8081/')

        //add second
        cy.get('[href="/task/add"]')
            .click()
        cy.get('#summary')
            .type('Marathon')
        cy.get('#detail')
            .type('Wing lai lung')
        cy.get('#priority')
            .select('High')
        cy.get('#duedate')
            .type('2020-10-13')
        cy.get('.btn')
            .click()

        cy.get(':nth-child(2) > .card-footer > .row > :nth-child(2) > .nav > [cl=""] > .nav-link > .fas')
            .click()
        cy.get('.btn-danger').click()
        
        cy.get('h4').should('be.visible')
        cy.get('h4').should('contain.text','No tasks to display. Add one above.')
    })

    //case3 edited task
    it('edited a task',() => {
        cy.visit('http://localhost:8081/')

        //add one
        cy.get('[href="/task/add"]')
            .click()
        cy.get('#summary')
            .type('asdf')
        cy.get('#detail')
            .type('asdf')
        cy.get('#priority')
            .select('Normal')
        cy.get('#duedate')
            .type('2020-12-06')
        cy.get('.btn')
            .click()

        cy.get(':nth-child(1) > .nav-link > .fas')
            .click()
    
        //edited to second
        cy.get('#summary')
            .clear()
        cy.get('#summary')
            .type('Marathon')
        cy.get('#detail')
            .clear()
        cy.get('#detail')
            .type('Wing lai lung')
        cy.get('#priority')
            .select('High')
        cy.get('#duedate')
            .type('2020-10-13')
        cy.get('.btn')
            .click()
        
        cy.get('.card-title').should('contain.text','Marathon')
        cy.get('.card-text').should('contain.text','Wing lai lung')
        cy.get(':nth-child(2) > .badge').should('contain.text','High')
        cy.get('.card-header > .row > :nth-child(1)').should('contain.text','2020-10-13')

        cy.get(':nth-child(2) > .card-footer > .row > :nth-child(2) > .nav > [cl=""] > .nav-link > .fas')
            .click()
        cy.get('.btn-danger').click()
    })

    //case4 Mark as Complete
    it('Mark as Complete',() => {
        cy.visit('http://localhost:8081/')

        //add second
        cy.get('[href="/task/add"]')
            .click()
        cy.get('#summary')
            .type('Marathon')
        cy.get('#detail')
            .type('Wing lai lung')
        cy.get('#priority')
            .select('High')
        cy.get('#duedate')
            .type('2020-10-13')
        cy.get('.btn')
            .click()
        
        cy.get('h4 > .badge').should('contain.text','Incomplete')
        
        cy.get('.btn > .fas')
            .click()
        
        cy.get('h4 > .badge').should('contain.text','Complete') 
        cy.get('.btn > .fas').should('have.class','fas fa-redo-alt')   
    })
})