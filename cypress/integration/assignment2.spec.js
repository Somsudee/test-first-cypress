/// <reference types="Cypress"/>
describe('Fill the form test',() => {
    Cypress.on('uncaught:exception', (err, runnable) => {
        // returning false here prevents Cypress from
        //failing the test
        return false
    })

    it('Currency test THB-THB',() => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
        cy.get('#sourceAmount')
            .type('50')
        cy.get('#calBtn')
            .click()

    })

    it('Currency test THB-USD',() => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
        cy.get('#selectSource')
            .click()
        cy.get('#sourceUSD')
            .click()
        cy.get('#sourceAmount')
            .type('20')
        cy.get('#calBtn')
            .click()
    })

    it('Currency test THB-EUR',() => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
        cy.get('#selectSource')
            .click()
        cy.get('#sourceEUR')
            .click()
        cy.get('#sourceAmount')
            .type('10')
        cy.get('#calBtn')
            .click()
    })

//usd
    it('Currency test USD-THB',() => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
        cy.get('#selectTarget')
            .click()
        cy.get('#targetUSD')
            .click()
        cy.get('#sourceAmount')
            .type('80')
        cy.get('#calBtn')
            .click()
    })

    it('Currency test USD-USD',() => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
        cy.get('#selectSource')
            .click()
        cy.get('#sourceUSD')
            .click()
        cy.get('#selectTarget')
            .click()
        cy.get('#targetUSD')
            .click()
        cy.get('#sourceAmount')
            .type('60')
        cy.get('#calBtn')
            .click()
    })

    it('Currency test USD-EUR',() => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
        cy.get('#selectSource')
            .click()
        cy.get('#sourceUSD')
            .click()
        cy.get('#selectTarget')
            .click()
        cy.get('#targetEUR')
            .click()
        cy.get('#sourceAmount')
            .type('70')
        cy.get('#calBtn')
            .click()
    })

//EUR
    it('Currency test EUR-THB',() => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
        cy.get('#selectTarget')
            .click()
        cy.get('#targetEUR')
            .click()
        cy.get('#sourceAmount')
            .type('90')
        cy.get('#calBtn')
            .click()
    })

    it('Currency test EUR-USD',() => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
        cy.get('#selectSource')
            .click()
        cy.get('#sourceUSD')
            .click()
        cy.get('#selectTarget')
            .click()
        cy.get('#targetEUR')
            .click()
        cy.get('#sourceAmount')
            .type('100')
        cy.get('#calBtn')
            .click()
    })

    it('Currency test EUR-EUR',() => {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
        cy.get('#selectSource')
            .click()
        cy.get('#sourceEUR')
            .click()
        cy.get('#selectTarget')
            .click()
        cy.get('#targetEUR')
            .click()
        cy.get('#sourceAmount')
            .type('120')
        cy.get('#calBtn')
            .click()
    })

})